const dotenv = require("dotenv");

const envFound = dotenv.config();
if (!envFound) {
  throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

const config = {
  server: {
    port: process.env.PORT || 8081,
    host: process.env.HOST || "localhost",
  },
  client: {
    url: process.env.CLIENT_URL_PORT || "http://localhost:3000",
  },
  database: {
    host: process.env.DB_HOST || "localhost",
    //   user: process.env.DB_USER || "root",
    //   password: process.env.DB_PASSWORD || "kaneki123",
    port: process.env.DB_PORT || 27017,
    name: process.env.DB_NAME || "chat_realtime",
  },
  firebase: {
    server_key:
      process.env.FIREBASE_SERVER_KEY ||
      "AAAAA8h0R7w:APA91bEOLPUyJZCScswkp96ZOwktPatGFg8mVsBzka7RCp1OdFIn51dlAW4F4npndrL9NYBUKboJfv6CrrAjvBDRR6DhVyZmy9IazA0FwiSxjw8CW2yg3v4awPxaFrvqt5caXQRUspiV",
  },
};

module.exports = config;
