const mongoose = require("mongoose");
const { database } = require("./config");

mongoose.connect(
  `mongodb://${database.host}:${database.port}/${database.name}`
);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
  console.log("Connected successfully");
});

module.exports = db;
