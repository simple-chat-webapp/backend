"use strict";

const config = require("../config/config");
const axios = require("axios");

let controller = {
  send: async (req, res) => {
    try {
      const { to, notification, data } = req.body;
      const { title, body, mutable_content, sound } = notification;
      const { url, dl } = data;

      const serverKey = config.firebase.server_key;

      const response = await axios.post(
        "https://fcm.googleapis.com/fcm/send",
        {
          to,
          notification: {
            title,
            body,
            mutable_content,
            sound,
          },
          data: {
            url,
            dl,
          },
        },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `key=${serverKey}`,
          },
        }
      );

      return res.json({
        status: "success",
        data: response.data,
      });
    } catch (err) {
      return res.status(500).json({
        status: "failed",
        message: err.message,
      });
    }
  },
};

module.exports = controller;
