const authController = require("../controllers/auth.controller");
const messageController = require("../controllers/message.controller");
const userController = require("../controllers/user.controller");
const firebaseController = require("../controllers/firebase.controller");

module.exports = {
  authController,
  messageController,
  userController,
  firebaseController,
};
