"use strict";

const { firebaseController } = require("../controllers/index");

module.exports = (router) => {
  router.group("/firebase", (router) => {
    router.post("/send-fcm-message", firebaseController.send);
  });
};
